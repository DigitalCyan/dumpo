import dotenv from 'dotenv';
dotenv.config();

import Ignitor from './Classes/Core/Ignitor';

Ignitor.instance.init();
