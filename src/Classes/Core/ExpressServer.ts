import express, {Request, Response} from 'express';
import FileManager from './FileManager';
import chokidar from 'chokidar';

export default class ExpressServer {
    private static _instance: ExpressServer;
    public static get instance(): ExpressServer {
        if (!this._instance) {
            this._instance = new ExpressServer();
        }
        return this._instance;
    }

    private app: express.Express | null = null;
    private response = '';

    public init(): void {
        this.response = "xD";
        this.app = express();
        this.updateResponse();

        const watcher = chokidar.watch(FileManager.instance.dataDir);
        watcher.on('add', this.updateResponse.bind(this));
        watcher.on('unlink', this.updateResponse.bind(this));

        this.app.use('/data', express.static(FileManager.instance.dataDir));
        this.app.get('/', this.requestHandler.bind(this));

        this.app.listen(process.env.PORT);
    }

    private updateResponse() {
        this.response = FileManager.instance.getHtml();
    }

    private requestHandler(req: Request, res: Response) {
        res.send(this.response);
    }
}
