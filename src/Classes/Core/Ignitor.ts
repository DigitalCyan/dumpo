import ExpressServer from "./ExpressServer";
import FileManager from "./FileManager";

export default class Ignitor {
    private static _instance: Ignitor;
    public static get instance(): Ignitor {
        if (!this._instance) {
            this._instance = new Ignitor();
        }
        return this._instance;
    }

    public init(): void {
        FileManager.instance.init();
        ExpressServer.instance.init();
    }
}
