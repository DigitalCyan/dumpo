import os from 'os';
import fs, { Stats } from 'fs';
import path from 'path';

export default class FileManager {
    private static _instance: FileManager;
    public static get instance(): FileManager {
        if (!this._instance) {
            this._instance = new FileManager();
        }
        return this._instance;
    }

    private hostname = os.hostname();
    public baseDir = '';
    public dataDir = '';

    public init() {
        this.baseDir = path.join(__dirname, '../../../');
        this.dataDir = path.join(this.baseDir, 'files');
    }

    public getFiles(): string[] {
        const files = fs.readdirSync(this.dataDir);
        return files;
    }

    private getShortTime(date: Date): string {
        return `${date.getDate()}.${date.getMonth()}.${date.getFullYear()}. - ${date.getHours()}:${date.getMinutes()}`;
    }

    public getHtml(): string {
        let response = '';
        response += `<title>${process.env.TITLE}</title>`;
        response += '<table border="1" style="border-collapse: collapse">';
        response += '<th>File name</th>';
        response += '<th>Size</th>';
        response += '<th>Created</th>';
        response += '<th>Last modified</th>';
        for (const file of this.getFiles()) {
            if (file[0] == '.') {
                continue;
            }

            const stats = fs.statSync(path.join(this.dataDir, file));
            if (!stats.isFile()) {
                continue;
            }

            response += '<tr>';
            response += `<td style="padding: 3px 1rem"><a href="/data/${file}" download>${file}</a></td>`;
            response += `<td style="padding: 3px 1rem">${Math.trunc(stats.size / 1048576)} MB</td>`;
            response += `<td style="padding: 3px 1rem">${this.getShortTime(stats.birthtime)}</td>`;
            response += `<td style="padding: 3px 1rem">${this.getShortTime(stats.mtime)}</td>`;
            response += '</tr>';
        }
        response += '</table>';
        return response;
    }
}
