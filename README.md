# dumpo

## About
dumpo is a quick and simple to use file hosting server that generates a HTML table of your hosted contents. To host files, simply put them into a directory called files located at the root of the project.

## Setup
Clone the repo and move into it
```
git clone https://gitlab.com/DigitalCyan/dumpo
cd dumpo
```

Treat yourself with a nice `.env`. Check `.env.example` for refrence.

`.env` example:
```
TITLE=My Very Cool Files :D
PORT=80
```

This will set some enviornment variables that the server will use.

Now install the dependencies using `yarn`
```
yarn install
```

Alright it's time to run the server
```
yarn start
```

After confirming that everything works (by opening your web browser and going to http://localhost\[:\<PORT\>\]), it's time to actually host something.
```
files
├── hello_world.py
├── illegal-linux-isos.tar.gz
├── my_stuff.rar
└── poggers.zip
```

Also keep in mind that that hidden files (the ones whos names begin with a dot) will not be displayed. So if you want to back up your dotfiles, just zip em (cause we don't support direcotries either).

## That's it!

You are now fully qualified to operate this piece of software, if you wish to contribute for some reason feel free to leave a pull request!